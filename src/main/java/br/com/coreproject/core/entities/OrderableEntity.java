package br.com.coreproject.core.entities;

public interface OrderableEntity {

    int getDisplayOrder();

    void setDisplayOrder(int displayOrder);

}
