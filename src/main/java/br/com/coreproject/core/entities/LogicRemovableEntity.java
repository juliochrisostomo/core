package br.com.coreproject.core.entities;

public interface LogicRemovableEntity {

    boolean isRemoved();

    void setRemoved(boolean removed);

}
