package br.com.coreproject.core.dto;

import lombok.Data;

@Data
public class Message {

    private boolean success;

    private String message;

    private Object data;

    public Message(String message) {
        this.message = message;
    }
}
