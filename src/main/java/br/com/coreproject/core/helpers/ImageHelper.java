package br.com.coreproject.core.helpers;

import org.imgscalr.Scalr;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ImageHelper {

    private BufferedImage source;

    private Scalr.Method method;

    private Scalr.Mode mode;

    private List<Dimension> dimensions;

    private Coordinates coordinates;

    public List<BufferedImage> resize() {
        if (dimensions.isEmpty()) {
            throw new IllegalArgumentException("At least a dimension must be specified");
        }

        return dimensions.stream()
            .map(d -> Scalr.resize(source, method, mode, d.width, d.height))
            .collect(Collectors.toList());
    }

    public BufferedImage crop() {
        if (dimensions.isEmpty()) {
            throw new IllegalArgumentException("At least a dimension must be specified");
        }

        Dimension dimension = dimensions.get(0);

        return Scalr.crop(source, coordinates.x, coordinates.y, dimension.width, dimension.height);
    }

    public BufferedImage flipVertically() {
        return Scalr.rotate(source, Scalr.Rotation.FLIP_VERT);
    }

    public BufferedImage flipHorizontally() {
        return Scalr.rotate(source, Scalr.Rotation.FLIP_HORZ);
    }

    public BufferedImage rotate90() {
        return Scalr.rotate(source, Scalr.Rotation.CW_90);
    }

    public BufferedImage rotate180() {
        return Scalr.rotate(source, Scalr.Rotation.CW_180);
    }

    public BufferedImage rotate270() {
        return Scalr.rotate(source, Scalr.Rotation.CW_270);
    }

    public static Builder builder() {
        return new ImageHelper.Builder();
    }

    public static class Builder {

        private ImageHelper instance = new ImageHelper();

        private Builder() {
            instance.method = Scalr.Method.AUTOMATIC;
            instance.mode = Scalr.Mode.AUTOMATIC;
            instance.dimensions = new ArrayList<>();
            instance.coordinates = new Coordinates(0, 0);
        }

        public Builder source(BufferedImage source) {
            instance.source = source;
            return this;
        }

        public Builder withMethod(Scalr.Method method) {
            instance.method = method;
            return this;
        }

        public Builder toDimension(int width, int height) {
            instance.dimensions.add(new Dimension(width, height));
            return this;
        }

        public Builder withCoordinates(int x, int y) {
            instance.coordinates = new Coordinates(x, y);
            return this;
        }

        public ImageHelper build() {
            if (instance.source == null) {
                throw new IllegalArgumentException("Source cannot be null");
            }

            return instance;
        }

    }

    private static class Dimension {
        int width, height;

        public Dimension(int width, int height) {
            this.width = width;
            this.height = height;
        }
    }

    private static class Coordinates {
        int x, y;

        public Coordinates(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

}
