package br.com.coreproject.core.repositories.specifications;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CoreSpecification<T> implements Specification<T> {

    private SearchCriteria criteria;

    public CoreSpecification(SearchCriteria criteria) {
        this.criteria = criteria;
    }

    @Nullable
    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

        String key = criteria.getKey();
        String operation = criteria.getOperation();
        Object value = criteria.getValue();

        Class<?> clazz = root.get(key).getJavaType();

        value = transform(clazz, value);

        switch (operation) {
            case ">":
                return builder.greaterThanOrEqualTo(root.get(key), value.toString());
            case "<":
                return builder.lessThanOrEqualTo(root.get(key), value.toString());
            case ":":
                if (value instanceof List) {

                    return builder.and(root.get(key).in(value));

                } else if (clazz == String.class) {

                    return builder.like(root.get(key), "%" + value + "%");


                } else {

                    return builder.equal(root.get(key), value);

                }
        }

        return null;

    }

    private Object transform(Class<?> clazz, Object value) {

        if (String.valueOf(value).matches("\\[[\\w,]+\\]")) {
            value = String.valueOf(value).replaceAll("\\[", "").replaceAll("\\]", "");

            if (String.valueOf(value).matches("[\\d,]+")) {
                value = Arrays.stream(String.valueOf(value).split(","))
                        .map(Long::parseLong)
                        .collect(Collectors.toList());
            } else {
                value = Arrays.asList(String.valueOf(value).split(","));
            }

            if (clazz.isEnum()) {

                value = ((List<String>)value).stream()
                            .map(v -> Enum.valueOf((Class<? extends Enum>) clazz, v))
                            .collect(Collectors.toList());

            }

        } else if (clazz.isEnum()) {

            value = Enum.valueOf((Class<? extends Enum>) clazz, String.valueOf(value));

        }

        return value;

    }

}
