package br.com.coreproject.core.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class JdbcTemplateRepository {

    private JdbcTemplate template;

    @Autowired
    public void init(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    public void sortBatch(Class<?> clazz, final List<Object[]> values) {

        if (values == null || values.isEmpty()) {
            return;
        }

        String sql = String.format("update %s set displayOrder = :displayOrder where id = :id", clazz.getName());

        template.batchUpdate(sql, values);

    }

}
