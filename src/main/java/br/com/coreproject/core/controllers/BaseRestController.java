package br.com.coreproject.core.controllers;

import br.com.coreproject.core.controllers.exceptions.InvalidBodyException;
import br.com.coreproject.core.entities.LogicRemovableEntity;
import br.com.coreproject.core.repositories.CoreRepository;
import br.com.coreproject.core.repositories.JdbcTemplateRepository;
import br.com.coreproject.core.repositories.specifications.SpecificationBuilder;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public interface BaseRestController<T, ID> {

    CoreRepository<T, ID> getRepository();

    JdbcTemplateRepository getJdbcTemplateRepository();

    Class<T> getEntityClass();

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    default void post(HttpServletRequest request, HttpServletResponse response, @Valid @RequestBody T entity, Errors errors) {

        validateBody(errors);

        getRepository().save(entity);

    }

    @PutMapping
    default void put(HttpServletRequest request, HttpServletResponse response, @Valid @RequestBody T entity, Errors errors) {

        validateBody(errors);

        getRepository().save(entity);

    }

    @PutMapping("/sort")
    default void sort(@RequestParam List<ID> ids, Pageable pageable) {

        int offset = Long.valueOf(pageable.getOffset()).intValue();
        int pageSize = pageable.getPageSize();

        List<Object[]> values = new ArrayList<>();

        for (ID id : ids) {
            values.add(new Object[]{ id, offset++ });
        }

        getJdbcTemplateRepository().sortBatch(getEntityClass(), values);

    }

    @DeleteMapping("/{id}")
    default void delete(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") ID id) {

        T entity;

        if ((entity = getRepository().getOne(id)) != null) {

            if (entity instanceof LogicRemovableEntity) {

                ((LogicRemovableEntity)entity).setRemoved(true);

                getRepository().save(entity);

            } else {

                getRepository().deleteById(id);

            }

        }

    }

    @GetMapping("/{id}")
    default T get(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") ID id) {
        return getRepository().getOne(id);
    }

    @GetMapping
    default List<T> get(HttpServletRequest request, HttpServletResponse response, String search) {

        if (search == null) {
            return getRepository().findAll();
        }

        SpecificationBuilder builder = new SpecificationBuilder();
        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\[[\\w,]+\\]|\\w+?);");
        Matcher matcher = pattern.matcher(search + ";");
        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
        }

        Specification<T> specification = builder.build();
        return getRepository().findAll(specification);

    }

    default void validateBody(Errors errors) {

        if (errors.hasErrors()) {

            String message = errors.getAllErrors().stream()
                    .map(e -> e.getDefaultMessage())
                    .collect(Collectors.joining(","));

            throw new InvalidBodyException(message);

        }

    }

}
