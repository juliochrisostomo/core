package br.com.coreproject.core.controllers;

import br.com.coreproject.core.entities.BaseFile;
import br.com.coreproject.core.repositories.CoreRepository;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public interface BaseFileController<F extends BaseFile> {

    Class<F> getFileClass();

    String getDirectory();

    CoreRepository getFileRepository();

    ResourceLoader getResourceLoader();

    @PostMapping("/upload")
    @ResponseStatus(HttpStatus.CREATED)
    default F upload(HttpServletRequest request, @RequestParam("file") MultipartFile file, @RequestParam(required = false) List<String> sizes) throws IOException, IllegalAccessException, InstantiationException {

        F f = writeFiles(file, sizes);

        return (F) getFileRepository().save(f);

    }

    default F writeFiles(MultipartFile multipartFile, List<String> sizes) throws IOException, InstantiationException, IllegalAccessException {

        Resource resource = getResourceLoader().getResource("classpath:/static");

        Path directoryPath = Paths.get(String.format("%s/upload/%s", resource.getFile().getCanonicalPath(), getDirectory()));

        String filePath = String.format("/static/upload/%s", getDirectory());

        if (!Files.exists(directoryPath)) {
            Files.createDirectory(directoryPath);
        }

        String originalFileName = multipartFile.getOriginalFilename();
        String extension = originalFileName.substring(originalFileName.lastIndexOf(".") + 1);
        String fileName = String.format("%s.%s", UUID.randomUUID().toString(), extension);

        Path path = directoryPath.resolve(fileName);

        Files.copy(multipartFile.getInputStream(), path);

        F entity = convertMultipartFile(getFileClass(), multipartFile);

        entity.setName(fileName);
        entity.setPath(String.format("%s/%s", filePath, fileName));
        setDimensionsFromInputStream(entity, multipartFile.getInputStream());

        return entity;

    }

    default F convertMultipartFile(Class<F> clazz, MultipartFile multipartFile) throws IllegalAccessException, InstantiationException {

        F file = clazz.newInstance();

        file.setMimeType(multipartFile.getContentType());
        file.setOriginalName(multipartFile.getOriginalFilename());
        file.setSize(multipartFile.getSize());

        return file;

    }

    default void setDimensionsFromInputStream(F entity, InputStream inputStream) {

        if (entity.getMimeType().contains("image")) {

            try {

                BufferedImage image = ImageIO.read(inputStream);

                entity.setDimensions(String.format("%dx%d", image.getWidth(), image.getHeight()));

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}
