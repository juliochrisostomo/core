package br.com.coreproject.core.repositories;

import br.com.coreproject.core.entities.File;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

@Repository
public interface FileRepository extends CoreRepository<File, Long> {

}
