package br.com.coreproject.core.repositories;

import br.com.coreproject.core.entities.Movie;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends CoreRepository<Movie, Long> {

}
