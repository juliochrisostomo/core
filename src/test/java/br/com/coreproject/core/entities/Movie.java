package br.com.coreproject.core.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Entity
@Where(clause = "removed = false")
@EqualsAndHashCode(callSuper = false)
public class Movie extends BaseEntity<Long> implements LogicRemovableEntity, OrderableEntity {

    private enum Genre {
        ACTION,
        DRAMA,
        COMEDY,
        ROMANCE
    }

    @NotBlank(message = "Nome do filme obrigatório")
    @Length(min = 1, max = 100, message = "Nome do filme deve ter de 1 a 100 caractéres")
    @Column(nullable = false, length = 100)
    private String name;

    @NotNull(message = "Data de lançamento obrigatória")
    @Column(nullable = false)
    private LocalDate releaseDate;

    @NotBlank(message = "Sinopse do filme obrigatório")
    @Lob
    @Column(nullable = false)
    private String synopse;

    @NotNull(message = "Genero do filme obrigatório")
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 10)
    private Genre genre;

    private boolean removed;

    private int displayOrder;

}