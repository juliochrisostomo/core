package br.com.coreproject.core.controllers;

import br.com.coreproject.core.entities.File;
import br.com.coreproject.core.entities.Movie;
import br.com.coreproject.core.repositories.FileRepository;
import br.com.coreproject.core.repositories.JdbcTemplateRepository;
import br.com.coreproject.core.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/movies")
public class MovieController implements BaseRestController<Movie, Long>, BaseFileController<File> {

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private MovieRepository repository;

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private JdbcTemplateRepository jdbcTemplateRepository;

    @Override
    public Class<Movie> getEntityClass() {
        return Movie.class;
    }

    @Override
    public Class<File> getFileClass() {
        return File.class;
    }

    @Override
    public MovieRepository getRepository() {
        return repository;
    }

    @Override
    public String getDirectory() {
        return "movies";
    }

    @Override
    public FileRepository getFileRepository() {
        return fileRepository;
    }

    @Override
    public JdbcTemplateRepository getJdbcTemplateRepository() {
        return jdbcTemplateRepository;
    }

    @Override
    public ResourceLoader getResourceLoader() {
        return resourceLoader;
    }

}
