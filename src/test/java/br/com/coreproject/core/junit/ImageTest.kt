package br.com.coreproject.core.junit

import br.com.coreproject.core.helpers.ImageHelper
import org.imgscalr.Scalr
import org.junit.Assert
import org.junit.Test
import java.net.URL
import javax.imageio.ImageIO

class ImageTests {

    private val imageToResizeUrl = "http://placehold.it/200.jpg"
    private val imageToRotateUrl = "http://placehold.it/350x150.jpg"

    @Test
    fun `Resize image with multiple dimensions`() {

        val source = ImageIO.read(URL(imageToResizeUrl))

        val resizedImages = ImageHelper.builder()
                .source(source)
                .withMethod(Scalr.Method.ULTRA_QUALITY)
                .toDimension(100, 100)
                .toDimension(50, 50)
                .build()
                .resize()

        Assert.assertEquals(resizedImages.size, 2)
        Assert.assertEquals(resizedImages[0].width, 100)
        Assert.assertEquals(resizedImages[0].height, 100)
        Assert.assertEquals(resizedImages[1].width, 50)
        Assert.assertEquals(resizedImages[1].height, 50)

    }

    @Test
    fun `Rotate image 90 degrees`() {

        val source = ImageIO.read(URL(imageToRotateUrl))

        val rotatedImage = ImageHelper.builder()
                .source(source)
                .build()
                .rotate90()

        Assert.assertNotNull(rotatedImage)
        Assert.assertEquals(rotatedImage.width, 150)
        Assert.assertEquals(rotatedImage.height, 350)

    }

    @Test
    fun `Rotate image 180 degrees`() {

        val source = ImageIO.read(URL(imageToRotateUrl))

        val rotatedImage = ImageHelper.builder()
                .source(source)
                .build()
                .rotate180()

        Assert.assertNotNull(rotatedImage)
        Assert.assertEquals(rotatedImage.width, 350)
        Assert.assertEquals(rotatedImage.height, 150)

    }

    @Test
    fun `Rotate image 270 degrees`() {

        val source = ImageIO.read(URL(imageToRotateUrl))

        val rotatedImage = ImageHelper.builder()
                .source(source)
                .build()
                .rotate270()

        Assert.assertNotNull(rotatedImage)
        Assert.assertEquals(rotatedImage.width, 150)
        Assert.assertEquals(rotatedImage.height, 350)

    }

    @Test
    fun `Crop image`() {

        val source = ImageIO.read(URL(imageToResizeUrl))

        val croppedImage = ImageHelper.builder()
                .source(source)
                .toDimension(100, 100)
                .withCoordinates(50, 50)
                .build()
                .crop()

        Assert.assertNotNull(croppedImage)
        Assert.assertEquals(croppedImage.width, 100)
        Assert.assertEquals(croppedImage.height, 100)

    }

}
